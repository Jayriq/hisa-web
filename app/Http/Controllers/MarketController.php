<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Market;

class MarketController extends Controller
{
  
  public function index()
  {
    $breadcrumbs = [
      ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Datatable"], ['name' => "Advanced"]
    ];

    // $users = User::get();
    return view('/markets', [
      'breadcrumbs' => $breadcrumbs
    ]);
  }


  public function all()
  {

    $markets = Market::get();
    
    return ['data' => $markets];
  }
}
