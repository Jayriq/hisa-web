<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
  
  public function index()
  {
    $breadcrumbs = [
      ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Datatable"], ['name' => "Advanced"]
    ];

    // $users = User::get();
    return view('/users', [
      'breadcrumbs' => $breadcrumbs
    ]);
  }


  public function users()
  {

    $users = User::get();
    
    return ['data' => $users];
  }
}
