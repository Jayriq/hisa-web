<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Validator;

class AuthController extends Controller
{
  /**
   * Create user
   *
   * @param  [string] name
   * @param  [string] email
   * @param  [string] password
   * @param  [string] password_confirmation
   * @return [string] message
   */
  public function register(Request $request)
  {
    $request->validate([
      'name' => 'required|string',
      'email' => 'required|string|email|unique:users',
      'password' => 'required|string|',
      'c_password' => 'required|same:password',
    ]);

    $user = new User([
      'name' => $request->name,
      'email' => $request->email,
      'password' => bcrypt($request->password)
    ]);
    if ($user->save()) {
      return response()->json([
        'message' => 'Successfully created user!'
      ], 201);
    } else {
      return response()->json(['error' => 'Provide proper details']);
    }
  }

  /**
   * Login user and create token
   *
   * @param  [string] email
   * @param  [string] password
   * @param  [boolean] remember_me
   * @return [string] access_token
   * @return [string] token_type
   * @return [string] expires_at
   */
  public function login(Request $request)
  {
    $credentials = $request->only('phone_number', 'password');

    if ($token = $this->guard()->attempt($credentials)) {

        $user = Auth::getLastAttempted();
        if ($user->is_admin==1) {
            $result = $this->respondWithToken($token);
            return redirect('/overview')->with( ['user' => $user] );
        }
        else{
            Auth::logout(); 
            return redirect('/')->with('flash_msg','Login failed. Kindly use your mobile app instead.');
        }
    } 
    else {        
    // validation not successful, send back to form 
        return redirect('/')->with('error_msg','Invalid login information, try again');
    }
  }

  /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
              // Get the user data.
        $user = $this->guard()->user();

        $response = User::where('id', $user->id)
            // ->with('questions', 'followTopics', 'answers')
            // ->withCount(['questions', 'answers', 'followers'])
            ->get();

        return response()->json([
            'status' => 200,
            'message' => 'Authorized.',
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => $response
            // 'user' => array(
            //     'id' => $user->id,
            //     'name' => $user->name,
            //     'phone_number' => $user->phone_number,
            //     'avatar' => $user->avatar
            // )
        ], 200);
    }

 
  /**
   * Logout user (Revoke the token)
   *
   * @return [string] message
   */
  public function logout(Request $request)
  {
    $request->user()->token()->revoke();
    // Auth::logout(); 
    return response()->json([
      'message' => 'Successfully logged out'
    ]);
  }

  /**
   * Get the authenticated User
   *
   * @return [json] user object
   */
  public function user(Request $request)
  {
    return response()->json($request->user());
  }
}
