<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Investor;

class InvestorController extends Controller
{
  
  public function index()
  {
    $breadcrumbs = [
      ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Datatable"], ['name' => "Advanced"]
    ];

    // $users = User::get();
    return view('/investors', [
      'breadcrumbs' => $breadcrumbs
    ]);
  }


  public function all()
  {

    $investors = Investor::get();
    
    return ['data' => $investors];
  }
}
