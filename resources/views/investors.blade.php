
@extends('layouts/contentLayoutMaster')

@section('title', 'Investors')

@section('vendor-style')
  {{-- vendor css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" type="text/css" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
<style>
    @media screen and (min-width: 676px) {
        .modal-dialog {
          max-width: 650px; /* New width for default modal */
        }
    }
</style>
@endsection


@section('content')

<section id="responsive-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header border-bottom">
          <h4 class="card-title">Investors</h4>
        </div>
        <div class="card-datatable">
          <table class="dt-responsive table">
            <thead>
              <tr>
                <!-- <th><img src="" alt="Avatar" width="32" height="32"></th> -->
                <th>Name</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>ID Type</th>
                <th>ID Number</th>
                <th>KRA PIN</th>
                <th>Nationality</th>
                <th>Resident Country</th>
                <th>Address</th>
                <th>Occupation</th>
                <th>ID Evidence (Front)</th>
                <th>ID EVidence (Back)</th>
                <th>Evidence (KRA PIN)</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <!-- <th><img src="" alt="Avatar" width="32" height="32"></th> -->
                <th>Name</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>ID Type</th>
                <th>ID Number</th>
                <th>KRA PIN</th>
                <th>Nationality</th>
                <th>Resident Country</th>
                <th>Address</th>
                <th>Occupation</th>
                <th>ID Evidence (Front)</th>
                <th>ID EVidence (Back)</th>
                <th>Evidence (KRA PIN)</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<!--/ Responsive Datatable -->
@endsection


@section('vendor-script')
{{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <!-- <script src="{{ asset(mix('js/scripts/tables/table-datatables-advanced.js')) }}"></script> -->
  <script type="text/javascript">
    $(function () {

      var dt_responsive_table = $('.dt-responsive');

      console.log(dt_responsive_table.length);
      if (dt_responsive_table.length) {
        var dt_ajax = dt_responsive_table.dataTable({
          // processing: true,
          ajax: "{{ route('investors.all') }}",
          columns: [
            // { data: 'picture' },
            { data: 'full_name' },
            { data: 'email' },
            { data: 'phone_number' },
            { data: 'identification_type' },
            { data: 'identification_number' },
            { data: 'tax_id_number' },
            { data: 'nationality' },
            { data: 'resident_country' },
            { data: 'address' },
            { data: 'occupation' },
            { 
              data: 'identification_front',
              render: function(data, type, full, meta) {
                    return type === 'display' ?
                        '<img src="' + data + '" width="300" height="200" />' :
                        data;
              } 
            },
            { 
              data: 'identification_back',
              render: function(data, type, full, meta) {
                    return type === 'display' ?
                        '<img src="' + data + '" width="300" height="200" />' :
                        data;
              }
            },
            { 
              data: 'tax_document',
              render: function(data, type, full, meta) {
                    return type === 'display' ?
                        '<img src="' + data + '" width="300" height="500" />' :
                        data;
              } 

            }
          ],
          columnDefs: [
            {
              className: 'control',
              orderable: false,
              targets: 0
            },
            {
              // Label
              targets: -1,
              render: function (data, type, full, meta) {
                var $status_number = full['status'];
                var $status = {
                  1: { title: 'Active', class: 'badge-light-success' },
                  0: { title: 'Inactive', class: ' badge-light-info' }
                };
                if (typeof $status[$status_number] === 'undefined') {
                  return data;
                }
                return (
                  '<span class="badge badge-pill ' +
                  $status[$status_number].class +
                  '">' +
                  $status[$status_number].title +
                  '</span>'
                );
              }
            }
          ],
          dom:
            '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
          responsive: {
            details: {
              display: $.fn.dataTable.Responsive.display.modal({
                header: function (row) {
                  var data = row.data();
                  return 'Details of ' + data['full_name'];
                }
              }),
              type: 'column',
              renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                tableClass: 'table'
              })
            }
          },
          language: {
            paginate: {
              // remove previous & next text from pagination
              previous: '&nbsp;',
              next: '&nbsp;'
            }
          }
        });
      }
    // // Filter form control to default size for all tables
    $('.dataTables_filter .form-control').removeClass('form-control-sm');
    $('.dataTables_length .custom-select').removeClass('custom-select-sm').removeClass('form-control-sm');
  });
</script>
@endsection
