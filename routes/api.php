<?php

use Illuminate\Http\Request;

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {

	// Route::middleware('auth:api')->get('/user', function (Request $request) {
	//     return $request->user();
	// });

	// Route::group([
	//   'prefix' => 'auth'
	// ], function () {
	  $api->post('login', 'App\Http\Controllers\AuthController@login');
	  $api->post('register', 'App\Http\Controllers\AuthController@register');

	  // $api->group([
	  //   'middleware' => 'auth:api'
	  // ], function () {
	  //   $api->get('logout', 'AuthController@logout');
	  //   $api->get('user', 'AuthController@user');
	  // });
	// });

});


$api->version('v1', ['middleware' => 'api'], function ($api) {
	$api->get('logout', 'App\Http\Controllers\AuthController@logout');
	$api->get('user', 'App\Http\Controllers\AuthController@user');
});